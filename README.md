# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Milleks on see repositoorium loodud? ###

See repositoorium on loodud, et jagada aine OOP raames loodud rühma tööd. Antud aine raames tegid Meelis Utt ja Martin Kivilo rakenduse, mis võimaldab kasutajatel võrgu
kaudu saata üksteisele tekst sõnumeid.

### Kuidas programmi käivitada? ###

Programmi tööks on vaja tõmmata kõik allalaadimis kaustas olevad failid.

Programmikoodi käivitamiseks on vajalik, et arvutis oleks Java failide kompileerimis võimalus.

# Kasutamine #

## Serveri poolel  
Vajalik käivitada pea klass SERVER_MAIN.java. Sellega on ka server käivitatud, ning programm asub kuulab sokklit 6066. Nüüd saavad
kliendid ennast serveriga ühendada. Kui klient on loonud ühenduse ilmub serveri akna paremasse serva kastike kasutaja IP aadressiga.
Võib juhtuda, et kliend ei saa serveriga ühendust, kuna arvuti tulemüür, kus toimib server programm, blokeerib programmiga loodavad ühendused. 

Serveri aknas on võimalik: saata klientidele sõnumeid, lugeda klientide vahel saadetavaid sõnumeid, eemaldada suhtlusest kliente, näha ühendatud klientide arvu,
kõigi ühendatud klientide IP aadresse ja puhastada vestluse teksti kuvav akna osa. Kui serveri programmi saatja soovid sõnumit edastada, tuleb see esiteks kirjutada
all servas olevasse teksti aknasse ja siis vajutada ENTER klahvi või SEND nuppu. Kogu vestlus kuvatakse programmi akna keskel paikneval osal. Sõnumite eemaldamiseks 
eelnimetatud aknast vajutada CLEAR klahvi. Ühendatud klientide arvu kuvatakse vestluse aknast üleval, ühendatud klientide aadresse aga programmi akna paremas
servas. Klientide aadressi kuvavate nupukest peale vajutades, vastav klient eemaldatakse. Serveri sulgemiseks, tuleb sulgeda programmi aken.

## Kliendi poolel 
Kliendi programmi käivitamine

Kliendi programmi käivitamiseks tuleb käivitada peaklass Projekt2.java. Programm küsib kasutajalt IP-aadressi, kuhu kasutaja ühendada soovib. Seejärel küsib programm kliendilt nime, millega klient ennast identifitseerida tahab (ei saa olla tühi nimi ehk ""). Peale seda avaneb kliendi aken ning kasutaja saab sõnumeid saatma hakata. Programm kuulab soklit 6066. 

Kliendi akna tutvustus

Kliendi aknas on teksti kirjutamisväli, lugemisväli ja 6 nuppu. Kirjutamisvälja saab kasutaja kirjutada oma sõnumi ning ENTERI-t vajutades saab sõnumit ära saata. Saadetud sõnum ilmub ka kasutaja lugemisvälja. Tühja sõnumit saata ei saa. Lugemisvälja ilmuvad kasutaja enda, teiste kasutajate ja serveri sõnumid. Lugemisväljal kuvatakse kliendi "nimi" ja kui server saadab sõnumi, siis kuvatakse, et tegemist on serveriga.  Nuppudeks on järgmised: DEFAULT, RAIN, SNAKE, BACKGROUND, FONT, STOP. 
DEFAULT: elimineerib animatsiooni ja taustapildi ning seab teksti värviks musta. 
RAIN: algab vihma animatsioon üle kirjutamis-ja lugemisvälja.
SNAKE: lugemisväljale ilmub algeline snake-mäng. Selleks et mängida, peab kasutaja vajutama lugemisväljale ning seejärel saab kasutaja noolenuppude abil madu liigutada (madu ise automaatselt ei liigu). Maol on võimalus ära süüa 50 toidupallikest. Peale 50 palli söömist ilmub lugemisväljale uued 50 palli. Mao kokkupõrge iseenda või seinaga mängu ei lõpeta. Seinast saab n-ö läbi minna: madu ilmub välja vastasseinast.
BACKGROUND: ilmub väike aken, milles on võimalus valida 4 pildi ja 4 gif vahel, mida lugemisvälja taustapildiks valida.
FONT: ilmub väike aken, milles on võimalus valida teksti värvi. Saab valida musta, sinise, punase ja rohelise värvi vahel.
STOP: lõpetab programmi töö. 

### Kelle poole pöörduda küsimustega? ###

Programmi loojad ja selle repositooriumi omanikud on Meelis Utt ja Martin Kivilo. Nende poole pöörduda küsimustega.