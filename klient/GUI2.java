import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import sun.nio.ch.Net;

import java.io.PrintWriter;

/**
 * Created by Meelis on 27.04.2017.
 */
public class GUI2 {
    private String NAME;
    private String IP;

    private TextArea textArea = new TextArea();
    private TextField textField = new TextField();

    private Connection2 NetWork;


    private double HEIGHT = 600;
    private double WIDTH = 620;

    private WithImages withImages;
    private FontColors fontColors;


    public GUI2() {
    }

    public Parent createContent() {
        Group root = new Group();
        Group sub_rain = new Group();
        Group sub2 = new Group();
        Group snakeNAM= new Group();

        textArea.setPrefHeight(550);
        textArea.setEditable(false);
        VBox BOXES = new VBox(20, textArea, textField);
        BOXES.setPrefSize(WIDTH, HEIGHT);

        Button DEFAULT = new Button("DEFAULT");
        Button op1 = new Button("RAIN");
        Button op2 = new Button("SNAKE");
        Button op3 = new Button("BACKGROUND");
        Button op4 = new Button("FONT COLOR");
        Button STOP = new Button("STOP");

        VBox BUTTONS = new VBox(20, DEFAULT, op1, op2, op3,op4, STOP);
        BUTTONS.setLayoutX(WIDTH);

        textArea.setFont(Font.font("Times New Roman", 16));

        withImages= new WithImages(textArea);
        fontColors=new FontColors(textArea);
        DEFAULT.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                sub_rain.getChildren().clear();
                sub2.getChildren().clear();
                snakeNAM.getChildren().clear();
                withImages.SetingImage("WHITE.png");
                textArea.setStyle("-fx-text-inner-color: black;");
            }
        });

        op1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                sub_rain.getChildren().clear();
                sub2.getChildren().clear();
                snakeNAM.getChildren().clear();

                for(int g=0;g<2;g++) {
                    for (int i = 0; i < 500; i++) {
                        Rain_v2 vahe = new Rain_v2(WIDTH, HEIGHT);
                        vahe.setSub_rain(sub_rain);
                        vahe.droplet(g*2000);
                    }
                }
            }
        });

        op2.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                snakeNAM.getChildren().clear();
                sub2.getChildren().clear();

                Snake snake = new Snake(WIDTH, HEIGHT);
                snake.setTextField(textArea);

                Circle snakePiece = snake.Snake();
                snake.setSnakeNAM(snakeNAM);
                snake.setSub2(sub2);
                snake.fillingFoodBag();

                sub_rain.getChildren().clear();

                snake.move();
                sub2.getChildren().addAll(snakePiece);
            }
        });

        op3.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                withImages.multiChoiceIm();

            }
        });
        op4.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                fontColors.changeFont();
            }
        });


        STOP.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                PrintWriter out = NetWork.getSend().getOut();
                out.println(NAME+" left the conversatsion");
                out.flush();
                System.exit(1);
            }
        });


        root.getChildren().addAll(BOXES, BUTTONS, sub_rain, sub2,snakeNAM);
        return root;
    }
    public void ipAdress(Stage stage) throws Exception {
        Stage forIP = new Stage();
        forIP.setTitle("IP-aadressi määramine");
        Label labelIP = new Label("Sisesta IP-aadress, millega ühendada soovid. ");
        TextField InputIP = new TextField();
        VBox kast = new VBox(labelIP, InputIP);
        Scene SceneIP = new Scene(kast,250,50);
        forIP.setScene(SceneIP);
        forIP.show();

        InputIP.setOnAction(event1->{
            try {
                IP = InputIP.getText();
                NetWork= new Connection2(IP);
                NetWork.connection();

                NetWork.getReceive().setTextArea(textArea);
                NetWork.getSend().setTextArea(textArea);
                NetWork.getSend().setTextField(textField);

                forIP.hide();
                name(stage);
            }
            catch(Exception e){
                System.out.println("Error occured inputting IP: "+ e);
            }
        });

    }

    public void name(Stage stage) throws Exception {
        Stage forName = new Stage();
        forName.setTitle("Nime määramine");
        Label label = new Label("Sisesta oma nimi. ");
        TextField InputName = new TextField();
        VBox box = new VBox(label, InputName);
        Scene NameScene = new Scene(box, 225, 50);
        forName.setScene(NameScene);

        forName.show();

        InputName.setOnAction(event1 -> {
            if (!(InputName.getText().equals(""))) {
                NAME = InputName.getText();
                NetWork.getSend().setNAME(NAME);
                forName.hide();
                stage.show();
            }
        });
    }
}