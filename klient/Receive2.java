import javafx.scene.control.TextArea;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Meelis on 27.04.2017.
 */
public class Receive2 implements Runnable{
    private Socket socket;
    private boolean isRunning;
    private String fromOther;
    private boolean received=false;
    private TextArea textArea;

    public Receive2(Socket socket, boolean isRunning) {
        this.socket = socket;
        this.isRunning = isRunning;
    }

    public void setTextArea(TextArea textArea) {
        this.textArea = textArea;
    }

    @Override
    public void run(){
        try{
            Scanner In = new Scanner(socket.getInputStream());
            while(isRunning){
                if(In.hasNextLine()){
                    received=true;
                    fromOther=In.nextLine();
                    textArea.appendText(fromOther+'\n');


                    if(fromOther.equals("none")){
                        isRunning=false;
                        System.out.println("Teine lõpetas vestluse");
                        System.exit(1);
                    }
                }
            }
        }
        catch(IOException e){
            System.out.println("Vastuvõtmisel juhtus viga: "+ e);
        }
    }
}