import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.util.Duration;
import static java.lang.Math.random;

/**
 * Created by Meelis on 1.05.2017.
 */
public class Rain_v2 {

        private double width;
        private double height;
        private Group sub_rain;


        public Rain_v2(double width, double height) {
            this.width = width;
            this.height = height;
        }

        public Line droplet(int delay){

            Line rainDrop = new Line();

            double x = random() * width;
            double y = random() * -2*height;
            double y1 = random() *35;
            rainDrop.setStartX(x);
            rainDrop.setStartY(y);
            rainDrop.setEndX(x);
            rainDrop.setEndY(y + y1);
            rainDrop.setStroke(Color.BLUE);

            sub_rain.getChildren().add(rainDrop);

            Timeline timeline= new Timeline();
            timeline.setDelay(Duration.millis(delay));
            timeline.setCycleCount(1000);
            timeline.getKeyFrames().add(new KeyFrame(Duration.millis(4000),new KeyValue(rainDrop.translateYProperty(),3*height+100)));
            timeline.play();

            return rainDrop;

        }

        public void setSub_rain(Group sub_rain) {
            this.sub_rain = sub_rain;
        }
    }