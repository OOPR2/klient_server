
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by Meelis on 27.04.2017.
 */
public class Projekt2 extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception{

        GUI2 KasutajaLiides = new GUI2();

        KasutajaLiides.ipAdress(stage);

        stage.setScene(new Scene(KasutajaLiides.createContent()));
        stage.setResizable(false);
    }

}