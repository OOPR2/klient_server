import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Meelis on 27.04.2017.
 */
public class Send2 implements Runnable{
    private Socket socket;
    private boolean isRunning;
    private TextField textField;
    private TextArea textArea;
    private String NAME;
    private String MESSAGE;
    private PrintWriter Out;


    public Send2(Socket socket, boolean isRunning) {
        this.socket = socket;
        this.isRunning = isRunning;
    }

    @Override
    public void run(){
        try {
            Out = new PrintWriter(socket.getOutputStream());
            while (isRunning) {
                textField.setOnAction(event -> {
                    if (!(textField.getText().equals(""))) {
                        String input = textField.getText();
                        textField.clear();
                        MESSAGE = NAME+ ": " + input;
                        textArea.appendText( "["+NAME+"]" + ": " + input + '\n');

                        Out.println(MESSAGE);
                        Out.flush();

                        if (input.equals("none")) {
                            isRunning = false;
                            Out.println("[" + NAME+ " left the conversation]");
                            System.exit(1);
                        }
                    }
                });
            }
        }
        catch(IOException e){
            System.out.println("Error occured while sending: " + e);

        }
    }

    public void setTextField(TextField textField) {
        this.textField = textField;
    }

    public void setTextArea(TextArea textArea) {
        this.textArea = textArea;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public PrintWriter getOut() {
        return Out;
    }
}