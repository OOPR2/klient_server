import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * Created by Meelis on 5.05.2017.
 */
public class WithImages {
    private TextArea textArea;

    public WithImages(TextArea textArea) {
        this.textArea = textArea;
    }
    public void SetingImage(String ChosenImage){
        Region region = (Region) textArea.lookup(".content");

        BackgroundImage image = new BackgroundImage(new Image(ChosenImage,800,600,false,true),
                BackgroundRepeat.REPEAT,BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        region.setBackground(new Background(image));
    }

    public void multiChoiceIm(){
        Group imNgif = new Group();
        Stage imageOPT = new Stage();
        imageOPT.setTitle("Background");


        RadioButton image1= new RadioButton("Image \"Java\"");
        RadioButton image2= new RadioButton("Image \"City\"");
        RadioButton image3= new RadioButton("Image \"Cherry Blossoms\"");
        RadioButton image4= new RadioButton("Image \"Night Sky\"");

        RadioButton gif1= new RadioButton("GIF \"Rain\"");
        RadioButton gif2= new RadioButton("GIF \"Night Sky\"");
        RadioButton gif3= new RadioButton("GIF \"Waterfall\"");
        RadioButton gif4= new RadioButton("GIF \"Lake\"");

        VBox imOp=new VBox(image1,image2,image3,image4);
        VBox gifOp = new VBox(gif1,gif2,gif3,gif4);
        gifOp.setLayoutY(85);

        imNgif.getChildren().addAll(imOp,gifOp);
        Scene imOpScene = new Scene(imNgif,250,160);
        imageOPT.setScene(imOpScene);
        imageOPT.show();

        image1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                SetingImage("IMAGEjava.jpg");
                imageOPT.hide();
            }
        });
        image2.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                SetingImage("IMAGEcity.jpg");
                imageOPT.hide();
                //textArea.
            }
        });
        image3.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                SetingImage("IMAGEblossoms.jpg");
                imageOPT.hide();
            }
        });
        image4.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                SetingImage("IMAGEnightsky.jpg");
                imageOPT.hide();
            }
        });
        gif1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                SetingImage("GIFrain.gif");
                imageOPT.hide();
            }
        });
        gif2.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                SetingImage("GIFnightsky.gif");
                imageOPT.hide();
            }
        });
        gif3.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                SetingImage("GIFnature.gif");
                imageOPT.hide();
            }
        });
        gif4.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                SetingImage("GIFwater.gif");
                imageOPT.hide();
            }
        });




    }
}