import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.input.KeyEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Meelis on 29.04.2017.
 */
public class Snake{
    private double width;
    private double height;
    private Circle piece;
    private TextArea textArea;
    private ArrayList<Circle> foodBag = new ArrayList<>();
    private Group snakeNAM;
    private Group sub2;
    private List<Circle> tailNhead = new ArrayList<>();
    private double previousHeight;
    private double previousWidth;

    public Snake(double width, double height) {
        this.width = width - 20;
        this.height = height - 70;
    }

    public Circle Snake() {
        piece = new Circle(5, Color.BLACK);
        piece.setCenterX(Math.random() * width);
        piece.setCenterY(Math.random() * height);
        tailNhead.add(piece);
        return piece;
    }

    public void move() {
        textArea.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.UP || event.getCode() == KeyCode.DOWN ||
                        event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT) {

                    previousHeight = tailNhead.get(tailNhead.size() - 1).getCenterY();
                    previousWidth = tailNhead.get(tailNhead.size() - 1).getCenterX();
                    follow(previousWidth, previousHeight);

                    if (event.getCode() == KeyCode.UP) {
                        if (piece.getCenterY() > (7.5)) {
                            piece.setCenterY(piece.getCenterY() - 10);
                        } else {
                            piece.setCenterY(height + 14.5);
                        }
                    }
                    if (event.getCode() == KeyCode.DOWN) {
                        if (piece.getCenterY() < (height + 7.5)) {
                            piece.setCenterY(piece.getCenterY() + 10);
                        } else {
                            piece.setCenterY(7.5);
                        }

                    }
                    if (event.getCode() == KeyCode.LEFT) {
                        if (piece.getCenterX() > (7.5)) {
                            piece.setCenterX(piece.getCenterX() - 10);
                        } else {
                            piece.setCenterX(width + 14.5);
                        }
                    }
                    if (event.getCode() == KeyCode.RIGHT) {
                        if (piece.getCenterX() < (width + 7.5)) {
                            piece.setCenterX(piece.getCenterX() + 10);
                        } else {
                            piece.setCenterX(7.5);
                        }
                    }

                    for (int i = 0; i < foodBag.size(); i++) {
                        if (piece.intersects(foodBag.get(i).getLayoutBounds())) {
                            foodBag.get(i).setVisible(false);
                            foodBag.remove(i);
                            Circle link = Tail(previousWidth, previousHeight);
                            tailNhead.add(link);
                            sub2.getChildren().add(link);

                        }
                        if (foodBag.size() == 0) {
                            foodBag.clear();
                            snakeNAM.getChildren().clear();
                            fillingFoodBag();
                        }
                    }
                }
            }
        });
    }

    public Circle food() {

        Circle food = new Circle(3.5, Color.rgb((int) Math.round(Math.random() * 255), (int) Math.round(Math.random() * 255), (int) Math.round(Math.random() * 255)));
        food.setCenterX(Math.random() * width);
        food.setCenterY(Math.random() * height);
        return food;
    }

    public void fillingFoodBag() {

        for (int i = 0; i < 50; i++) {
            Circle food_var = food();
            foodBag.add(food_var);
            snakeNAM.getChildren().add(food_var);
        }
    }

    public Circle Tail(double prevWidth, double prevHeight) {
        Circle tailPiece = new Circle(3.5, Color.BLACK);
        tailPiece.setCenterX(prevWidth);
        tailPiece.setCenterY(prevHeight);
        return tailPiece;
    }

    public void follow(double prevWidth, double prevHeight) {
        double x = prevWidth;
        double y = prevHeight;

        double prevX = 0;
        double prevY = 0;
        for (int i = 1; i < tailNhead.size(); i++) {
            if (i == 1) {
                prevX = tailNhead.get(i).getCenterX();
                prevY = tailNhead.get(i).getCenterY();
                tailNhead.get(i).setCenterX(tailNhead.get(i - 1).getCenterX());
                tailNhead.get(i).setCenterY(tailNhead.get(i - 1).getCenterY());
            } else {
                x = tailNhead.get(i).getCenterX();
                y = tailNhead.get(i).getCenterY();
                tailNhead.get(i).setCenterX(prevX);
                tailNhead.get(i).setCenterY(prevY);
                prevX = x;
                prevY = y;
            }
        }
    }


    public void setTextField(TextArea textArea) {
        this.textArea = textArea;
    }


    public void setFoodBag(ArrayList<Circle> foodBag) {
        this.foodBag = foodBag;
    }


    public void setSnakeNAM(Group snakeNAM) {
        this.snakeNAM = snakeNAM;
    }

    public void setSub2(Group sub2) {
        this.sub2 = sub2;
    }

}