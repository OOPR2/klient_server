import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by Meelis on 6.05.2017.
 */
public class FontColors {
    private TextArea textArea;

    public FontColors(TextArea textArea) {
        this.textArea = textArea;
    }

    public void changeFont(){
        Group font = new Group();
        Stage forFont = new Stage();
        forFont.setTitle("Font Color");

        RadioButton fontColor1= new RadioButton("Black");
        RadioButton fontColor2= new RadioButton("Red");
        RadioButton fontColor3= new RadioButton("Blue");
        RadioButton fontColor4= new RadioButton("Green");

        VBox foOp=new VBox(fontColor1,fontColor2,fontColor3,fontColor4);

        font.getChildren().addAll(foOp);
        Scene foOpScene = new Scene(font,250,75);
        forFont.setScene(foOpScene);
        forFont.show();
        fontColor1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                textArea.setStyle("-fx-text-inner-color: black;");
                forFont.hide();
            }
        });
        fontColor2.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                textArea.setStyle("-fx-text-inner-color: red;");
                forFont.hide();
            }
        });
        fontColor3.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                textArea.setStyle("-fx-text-inner-color: blue;");
                forFont.hide();
            }
        });
        fontColor4.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                textArea.setStyle("-fx-text-inner-color: green;");
                forFont.hide();
            }
        });
    }
}
