import java.io.IOException;
import java.net.Socket;

/**
 * Created by Meelis on 27.04.2017.
 */
public class Connection2 {
    private int port=6066;
    private Socket socket;
    private String IP;
    private boolean isRunning=true;

    private Send2 send;
    private Receive2 receive;

    public Connection2(String IP) {
        this.IP = IP;
    }

    public void connection(){
        try{
            socket = new Socket(IP,port);
            send = new Send2(socket,isRunning);
            receive= new Receive2(socket,isRunning);

            Thread forSending = new Thread(send);
            Thread forReceiving = new Thread(receive);

            forSending.start();
            forReceiving.start();
        }
        catch(IOException e){
            System.out.println("Error occured while connecting: " + e);
        }
    }

    public Send2 getSend() {
        return send;
    }

    public Receive2 getReceive() {
        return receive;
    }
}