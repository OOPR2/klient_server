import com.sun.javafx.tk.Toolkit;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
//import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Martin Kivilo on 01-May-17.
 */
public class GUI extends Application implements Runnable
{
    protected String MESSAGES;
    protected String SERVER_USER_NAME;
    protected static boolean SERVER_RUNNING;
    protected static CLIENT_MANAGER CLIENTS;
    protected static TextArea MESSAGES_FIELD;
    protected static Text CLIENT_COUNTER;
    protected static List<String[]> CLIENT_MESSAGES;
    protected static List<Button> CLIENT_BUTTONS;
    protected static BorderPane bp_CENTER;
    protected static int NUM_BUTTONS;

    public GUI()
    {
        NUM_BUTTONS=0;
        SERVER_USER_NAME="SERVER";
        SERVER_RUNNING=true;
        CLIENTS =new CLIENT_MANAGER();
    }

    public void run()
    {
        Thread text_area=new Thread(new GUI_UPDATER());
        bp_CENTER = new BorderPane();
        text_area.start();
        launch();
    }

    public void start(Stage primaryStage) throws IOException
    {
        primaryStage.setTitle("SERVER");
        Color CUSTOM_GRAY = Color.rgb(65,65,65);
        ScrollPane SCROLL_BUTTONS = new ScrollPane();
        Group SCROLL_BTN_GRP = new Group();
        BorderPane bp_MAIN = new BorderPane();
        BorderPane bp_TOP_CENTER = new BorderPane();
        BorderPane bp_TOP = new BorderPane();
        BorderPane bp_BOTTOM = new BorderPane();
        BorderPane bp_BOTTOM_RIGHT = new BorderPane();

        TextField INPUT_FIELD = new TextField ();

        Scene SCENE = new Scene(bp_MAIN, 800, 400);

        Text CLI_CON_TEXT =new Text("CLIENTS CONNECTED: ");

        //Label INPUT_FIELD_LABEL = new Label("INPUT");
        MESSAGES_FIELD = new TextArea();
        MESSAGES_FIELD.setEditable(false);
        MESSAGES_FIELD.textProperty();
        MESSAGES_FIELD.setId("-1");
        MESSAGES_FIELD.setWrapText(true);
        //MESSAGES_FIELD.setCursor(Cursor.MOVE);


        CLIENT_COUNTER = new Text("0");

        CLI_CON_TEXT.setFill(Color.WHITE);
        CLIENT_COUNTER.setFill(Color.WHITE);

        TextFlow TEXT_CONTAINER = new TextFlow(CLI_CON_TEXT,CLIENT_COUNTER);
        TEXT_CONTAINER.setStyle("-fx-background-color: black; -fx-border-color: black;");

        Button SEND_INPUT = new Button("Send");
        Button CLEAR = new Button("Clear Messages");
        SEND_INPUT.setStyle("-fx-background-color: white; -fx-border-color: black;");
        CLEAR.setStyle("-fx-background-color: white; -fx-border-color: black;");
        SEND_INPUT.setCursor(Cursor.HAND);
        CLEAR.setCursor(Cursor.HAND);

        SCROLL_BUTTONS.setContent(SCROLL_BTN_GRP);
        SCROLL_BUTTONS.setMinWidth(0);

        CLEAR.setOnAction(event ->
        {
            MESSAGES_FIELD.clear();
        });
        INPUT_FIELD.setOnAction(event ->
        {
            try
            {
                SEND_TEXT(INPUT_FIELD);
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        });
        SEND_INPUT.setOnAction(event ->
        {
            try
            {
                SEND_TEXT(INPUT_FIELD);
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        });
        Task UPDATE_SCROLL_PANE = new Task<Void>()
        {
            @Override public Void call() throws InterruptedException
            {
                while (SERVER_RUNNING)
                {
                    List<CLIENT> clients = CLIENTS.getClients();
                    int NUM_BUTTONS=0;
                    if(SCROLL_BTN_GRP.getChildren().size()!=clients.size())
                    {
                        Platform.runLater(() ->SCROLL_BTN_GRP.getChildren().clear());
                        for (CLIENT client : clients)
                        {
                            Button client_button = new Button();
                            client_button.setText(client.getAddress());
                            client_button.setMaxHeight(7);
                            client_button.setLayoutY(30 * NUM_BUTTONS);
                            client_button.setId(Integer.toString(client.getID()));
                            client_button.setStyle( "-fx-background-color: #00D002; " +
                                                    "-fx-border-width: 2px;"+
                                                    "-fx-border-color: black;");
                            client_button.setOnMouseClicked(event ->
                            {
                                final CLIENT clnt = client;
                                client_button.setStyle("-fx-background-color: red;");
                                clnt.disconnect();
                            });
                            Platform.runLater(() -> SCROLL_BTN_GRP.getChildren().add(client_button));
                            NUM_BUTTONS++;
                        }
                        Thread.sleep(1000);
                    }
                }
                return null;
            }
        };

        SCROLL_BUTTONS.vvalueProperty().bind(UPDATE_SCROLL_PANE.progressProperty());
        new Thread(UPDATE_SCROLL_PANE).start();

        bp_TOP_CENTER.setCenter(TEXT_CONTAINER);
        bp_TOP.setCenter(bp_TOP_CENTER);
        bp_CENTER.setCenter(MESSAGES_FIELD);
        bp_CENTER.setRight(SCROLL_BUTTONS);
        bp_BOTTOM_RIGHT.setLeft(SEND_INPUT);
        bp_BOTTOM_RIGHT.setRight(CLEAR);
        bp_BOTTOM.setCenter(INPUT_FIELD);
        bp_BOTTOM.setRight(bp_BOTTOM_RIGHT);
        bp_MAIN.setTop(bp_TOP);
        bp_MAIN.setCenter(bp_CENTER);
        bp_MAIN.setBottom(bp_BOTTOM);

        primaryStage.setOnCloseRequest(event ->
        {
            try
            {
                CLIENTS.shutDown();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
            SERVER_RUNNING=false;
        });
        SCENE.setFill(CUSTOM_GRAY);
        primaryStage.setScene(SCENE);
        primaryStage.show();
        Region MESSAGES_FIELD_CONTENT = ( Region ) MESSAGES_FIELD.lookup( ".content" );

        MESSAGES_FIELD.setStyle("-fx-text-fill: #14D300; -fx-font-family: monospace; -fx-focus-color: transparent;");
        MESSAGES_FIELD_CONTENT.setStyle("-fx-background-color: black;"+
                                        "-fx-border: none;"+
                                        "-fx-focus-color: transparent;");
    }
    private void SEND_TEXT(TextField INPUT_FIELD) throws IOException
    {
        if(!INPUT_FIELD.getText().equals(""))
        {
            MESSAGES+=INPUT_FIELD.getText()+"\n";
            MESSAGES_FIELD.appendText("["+SERVER_USER_NAME+"]: "+INPUT_FIELD.getText()+"\n");
            try
            {
                CLIENTS.sendMessageFromServer(INPUT_FIELD.getText());
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            INPUT_FIELD.setText("");
        }
    }
}
class GUI_UPDATER extends GUI implements Runnable
{
    public void run()
    {
        while (SERVER_RUNNING)
        {
            CLIENT_MESSAGES = CLIENTS.getMessages();
            if (CLIENT_MESSAGES!=null)
            {
                for (String[] message : CLIENT_MESSAGES)
                {
                    MESSAGES_FIELD.appendText("[" + message[2]+"@"+message[3]+ "]: " + message[1] + "\n");
                }
            }
            if(CLIENT_COUNTER!=null && CLIENTS.getNumberOfClients()!=Integer.parseInt(CLIENT_COUNTER.getText()))
            {
                CLIENT_COUNTER.setText(Integer.toString(CLIENTS.getNumberOfClients()));
            }
        }
    }
}
