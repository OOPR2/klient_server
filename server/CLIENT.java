import java.io.*;
import java.net.Socket;
import java.net.SocketException;

/**
 * Created by Martin Kivilo on 27-Apr-17.
 */
public class CLIENT
{
    private int ID;
    private String NAME;
    private String ADDRESS;
    private Socket SOCKET;
    private BufferedWriter OUT;
    private BufferedReader IN;
    private boolean CONNECTED;
    private boolean DEFAULT_NAME;

    public CLIENT(Socket SOCKET, int ID) throws IOException
    {
        DEFAULT_NAME=true;
        CONNECTED=true;
        this.SOCKET = SOCKET;
        OUT=new BufferedWriter(new OutputStreamWriter(SOCKET.getOutputStream()));
        IN=new BufferedReader(new InputStreamReader(SOCKET.getInputStream()));
        NAME=SOCKET.getRemoteSocketAddress().toString();
        ADDRESS=NAME.replace("/","");
        this.ID=ID;
    }
    public void sendMessage(String text) throws IOException
    {
        try
        {
            OUT.write(text);
            OUT.newLine();
            OUT.flush();
        }
        catch (SocketException e)
        {
            CONNECTED=false;
        }
    }

    public int getID()
    {
        return ID;
    }
    public String getName()
    {
        return NAME;
    }
    public String getAddress()
    {
        return ADDRESS;
    }
    private boolean isSpecialMessage(String MESSAGE)
    {
        if(MESSAGE.equals(""))
        {
            return true;
        }
        else if(DEFAULT_NAME)
        {
            DEFAULT_NAME=false;
            NAME=MESSAGE.split(":")[0];
            return false;
        }
        else
        {
            return false;
        }
    }
    public String[] getMessage() throws IOException
    {
        if(IN.ready())
        {
            String CLIENT_MESSAGE=IN.readLine();
            if(isSpecialMessage(CLIENT_MESSAGE))
            {
                return null;
            }
            String[] message_and_id={Integer.toString(ID),CLIENT_MESSAGE.substring(NAME.length()+1,CLIENT_MESSAGE.length())/*IN.readLine()/*IN.nextLine()*/,NAME,ADDRESS};
            return message_and_id;
        }
        else
        {
            return null;
        }
    }
    public void disconnect()
    {
        CONNECTED=false;
    }
    public boolean isConnected()
    {
        return CONNECTED;
    }
    public void close() throws IOException
    {
        OUT.close();
        SOCKET.close();
    }
}
