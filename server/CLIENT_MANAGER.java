import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Martin Kivilo on 01-May-17.
 */
public class CLIENT_MANAGER implements Runnable
{
    protected static List<CLIENT> CLIENTS;
    protected static List<String[]> MESSAGES;
    private int SERVER_PORT=6066;
    private static int NUMBER_OF_CLIENTS;
    protected static boolean SERVER_RUNNING;
    protected static ServerSocket SERVER;

    public void run()
    {
        SERVER_RUNNING=true;
        this.CLIENTS = new CopyOnWriteArrayList<CLIENT>();
        this.MESSAGES = new CopyOnWriteArrayList<String[]>();
        try
        {
            SERVER = new ServerSocket(SERVER_PORT);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        NUMBER_OF_CLIENTS=1;
        Thread RECEIVER = new Thread(new CLIENT_MANAGER_RECEIVER());
        RECEIVER.start();
        while (SERVER_RUNNING)
        {
            try
            {
                addClient(new CLIENT(SERVER.accept(),NUMBER_OF_CLIENTS));
            }
            catch (SocketException e)
            {
                if(e.getMessage().equals("socket closed"))
                {

                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            NUMBER_OF_CLIENTS++;
        }
    }
    public boolean isServerRunning()
    {
        return SERVER_RUNNING;
    }
    public int getNumberOfClients()
    {
        if(CLIENTS==null)
        {
            return -1;
        }
        else
        {
            return CLIENTS.size();
        }
    }
    public void addClient(CLIENT client)
    {
        CLIENTS.add(client);
    }
    public void sendMessage(String[] message) throws IOException
    {
        for(CLIENT client:CLIENTS)
        {
            if(client.getID()!=Integer.parseInt(message[0]))
            {
                client.sendMessage("["+message[2]+"]: "+message[1]);
            }
        }
    }
    public void sendMessageFromServer(String message) throws IOException
    {
        for (CLIENT client : CLIENTS)
        {
            client.sendMessage("[SERVER]: "+message);
        }
    }
    public void closeAllConnections() throws IOException
    {
        synchronized (CLIENTS)
        {
            for (CLIENT client : CLIENTS)
            {
                client.close();
            }
        }
    }
    public void shutDown() throws IOException
    {
        SERVER_RUNNING=false;
        closeAllConnections();
        SERVER.close();
    }
    public List<String[]> getMessages()
    {
        if(!this.MESSAGES.isEmpty())
        {
            List<String[]> messages = new CopyOnWriteArrayList<>(this.MESSAGES);
            this.MESSAGES.clear();
            return messages;
        }
        else
        {
            return null;
        }
    }
    public List<CLIENT> getClients()
    {
        return new CopyOnWriteArrayList<>(this.CLIENTS);
    }
    protected void updateMessages() throws IOException
    {
        for(CLIENT client:CLIENTS)
        {
            String[] message=client.getMessage();
            if(message!=null)
            {
                MESSAGES.add(message);
                sendMessage(message);
            }
        }
    }
    protected void removeNotActive()
    {
        for(int i=CLIENTS.size()-1;i>=0;i--)
        {
            if(!CLIENTS.get(i).isConnected())
            {
                try
                {
                    CLIENTS.get(i).close();
                } catch (IOException e)
                {
                    throw new RuntimeException("Kliendi eemaldamine ebaõnnestus!");
                }
                CLIENTS.remove(i);
            }
        }
    }
}
class CLIENT_MANAGER_RECEIVER implements Runnable
{
    private CLIENT_MANAGER MANAGER;

    public CLIENT_MANAGER_RECEIVER()
    {
        MANAGER=new CLIENT_MANAGER();
    }
    public void run()
    {
        while(MANAGER.isServerRunning())
        {
            try
            {
                MANAGER.removeNotActive();
                MANAGER.updateMessages();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
