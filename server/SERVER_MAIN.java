import java.io.IOException;

/**
 * Created by Martin Kivilo on 06-Apr-17.
 */
public class SERVER_MAIN
{
    public static void main(String[] args) throws IOException
    {
        CLIENT_MANAGER client = new CLIENT_MANAGER();
        Thread CLIENT=new Thread(client);
        CLIENT.start();
        Thread GRAPHIX= new Thread(new GUI());
        GRAPHIX.start();
    }
}
